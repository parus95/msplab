//***************************************************************************************
//  MSP430 Blink the LED Demo - Software Toggle P1.0
//
//  Description; Toggle P1.0 by xor'ing P1.0 inside of a software loop.
//  ACLK = n/a, MCLK = SMCLK = default DCO
//
//                MSP430x5xx
//             -----------------
//         /|\|              XIN|-
//          | |                 |
//          --|RST          XOUT|-
//            |                 |
//            |             P1.0|-->LED
//
//  J. Stevenson
//  Texas Instruments, Inc
//  July 2011
//  Built with Code Composer Studio v5
//***************************************************************************************

#include <msp430.h>				
#include <stdint.h>
#include <stdbool.h>

static volatile uint32_t SystemTime = 0;

static void DelayMs( uint32_t time )
{
	uint32_t await = SystemTime + time;
	while ((int32_t)(await - SystemTime) > 0){

	}
}

static inline uint8_t ButtonPressed( void ){
	return !(P1IN & BIT7);
}

static void WaitButtonRelease( void ) {
	uint8_t n;
	for (n = 0; n < 10; n++)
	{
		if (ButtonPressed())
		{
			n = 0;
		}
		else
		{
			DelayMs(25);
		}
	}
}

static inline void SetLed( uint8_t state ){
	if (state){
		P1OUT |= BIT0;
	} else {
		P1OUT &= ~BIT0;
	}
}

static uint8_t ReadDigit( void )
{
	uint8_t count = 0;


	SetLed(1);
	DelayMs(2000);
	SetLed(0);

	uint32_t lastPressTime = SystemTime;
	while ((int32_t)(SystemTime - lastPressTime) < 2000 && count < 10)
	{
		if (ButtonPressed()) {
			count ++;

			WaitButtonRelease();
			lastPressTime = SystemTime;
		}
	}

	SetLed(1);
	DelayMs(1500);
	SetLed(0);
	DelayMs(500);

	return count;
}

static void InitTimer( void ){
	  TA0CCTL0 = CCIE;                          // CCR0 interrupt enabled
	  TA0CCR0 = 500;
	  TA0CTL = TASSEL_2 + MC_1 + ID_0 + TACLR;
}

// Timer0 A0 interrupt service routine
#pragma vector=TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR(void)
{
	SystemTime++;
}

static void EnableInterrupt( void )
{
	__bis_SR_register(GIE);
}


#define SLIDER_MASK (((1<<5)-1)<<1)


void SliderUnlock( void )
{
	uint8_t sliderValue = SLIDER_MASK;
	do {
		sliderValue = (sliderValue >> 1) & SLIDER_MASK;

		P1OUT = (P1OUT & ~SLIDER_MASK) | sliderValue;

		DelayMs(500);
	} while (sliderValue);
}


static const uint8_t validKey[] = { 3, 5, 1, 7 };

bool ReadAndValidate( void ){
	uint8_t i, digit;
	for ( i = 0; i < sizeof(validKey) / sizeof(validKey[0]); i++){
		digit = ReadDigit();
		if (digit != validKey[i])
			return false;
	}
	return true;
}

int main(void)
{
  WDTCTL = WDTPW + WDTHOLD;             // Stop watchdog timer
  P1DIR |= BIT0;                        // Set P1.0 to output direction
  SetLed(0);

  //Button
  P1DIR &= ~BIT7;
  P1REN |=  BIT7;
  P1OUT |=  BIT7;

  //Slider
  P1DIR |= SLIDER_MASK;
  P1OUT |= SLIDER_MASK;


  InitTimer();
  EnableInterrupt();


  while (1){

	  if (ButtonPressed()){
		  WaitButtonRelease();
		  if (ReadAndValidate()){
			  SliderUnlock();
		  }
	  }
  }
}