
#include "msp430.h"

#include "HAL_PMM.h"
#include "HAL_UCS.h"

#include "HAL_Board.h"
#include "HAL_Wheel.h"

#include "consts.h"

#include "measure.h"
#include "hist.h"
#include "draw.h"

#define HIST_BARS 10
static Hist_TypeDef
  timeHist,
  accelHist[ACCEL_AXES];
static MeasureResult_TypeDef
  measureResult;

static void Repaint( uint8_t sel ) {
  if (sel > 3)
    return;
  const char * const titles[4] = {"Time", "X axis", "Y axis", "Z axis"};
  
  Hist_TypeDef const * hist = (sel == 0) ? &timeHist : &accelHist[sel - 1];
  const float data = (sel == 0) ? measureResult.time : measureResult.accel[sel - 1];
  
  Draw_Hist(hist, titles[sel]);
  
  Draw_Data(data);
}

void TryRepaintIfNeeded( uint8_t force ){
  static uint8_t selectedHist = 0;

  int position = Wheel_getPosition();
  force |= (position != selectedHist);
  if (force) {
    selectedHist = position;
    Repaint(selectedHist);
  }
}

void Tick( void ){
  TryRepaintIfNeeded(0);
}

int main( void )
{
  // Stop watchdog timer to prevent time out reset
  WDTCTL = WDTPW + WDTHOLD;

  Board_init();
  
  
    // Set Vcore to accomodate for max. allowed system speed
    SetVCore(3);

    // Use 32.768kHz XTAL as reference
    LFXT_Start(XT1DRIVE_0);

    // Set system clock to max (25MHz)
    Init_FLL_Settle(25000, 762);

    SFRIFG1 = 0;
    SFRIE1 |= OFIE;
    
  Draw_Init();

  Wheel_init();
    
  __enable_interrupt();

  Wheel_enable();
  
  Hist_Init(&timeHist, 0, 20, HIST_BARS);
  for (uint8_t i=0; i < ACCEL_AXES; i++){
    Hist_Init(&accelHist[i], -10, 10, HIST_BARS);
  }
  
  Measure_Init();

  while (1) {
    Measure_Do(&measureResult);
    
    Hist_Put(&timeHist, measureResult.time);
    
    for (uint8_t i=0; i < ACCEL_AXES; i++) {
        Hist_Put(&accelHist[i], measureResult.accel[i]);
    }

    TryRepaintIfNeeded(1);
  }
}
