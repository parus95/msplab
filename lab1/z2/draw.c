#include "draw.h"
#include "HAL_Dogs102x6.h"
#include "hist.h"
#include <stdio.h>

void Draw_Init( void )
{
  Dogs102x6_init();
  Dogs102x6_backlightInit();
  Dogs102x6_clearScreen();
}


void Draw_Hist(const Hist_TypeDef *hist, const char *title)
{
  Dogs102x6_clearScreen();
  Dogs102x6_setBacklight(8);
  
  //��, �����, �� ��� �������� ����������
  Dogs102x6_stringDraw(0, 0, (char *)title, DOGS102x6_DRAW_NORMAL);
  
  float max = hist->hits[0];
  for (uint8_t i=1; i< hist->intervals; i++)
    if (max < hist->hits[i])
      max = hist->hits[i];
  
  const int YOFFSET = 8;
  
  for (int x = 0; x < DOGS102x6_X_SIZE; x++){
    int columnN = x * hist->intervals / DOGS102x6_X_SIZE;
    
    int columnHeight = (int)(hist->hits[columnN] * (DOGS102x6_Y_SIZE - 2 * YOFFSET) / max);
      
    Dogs102x6_verticalLineDraw(DOGS102x6_Y_SIZE - YOFFSET, DOGS102x6_Y_SIZE - YOFFSET - columnHeight, x, DOGS102x6_DRAW_NORMAL);
  }
  
  char line[30];
  snprintf(line, 30, "M:%.3f S:%.3f", Hist_Mean(hist), Hist_Std(hist));
  Dogs102x6_stringDraw(7, 0, (char *)line, DOGS102x6_DRAW_NORMAL);
}
void Draw_Data(float value)
{
  char str[20];
  snprintf(str, 20, "%.4f", value);
  Dogs102x6_stringDraw(0, 7 * 7, str, DOGS102x6_DRAW_NORMAL);
  
}