#pragma once
#include <stdint.h>
#include "consts.h"

typedef struct {
  float time;
  float accel[ACCEL_AXES];
} MeasureResult_TypeDef;

void Measure_Init( void );
void Measure_Do(MeasureResult_TypeDef *result);
