#include <stdlib.h> //rand()

#include "measure.h"
#include "consts.h"

#include "HAL_Cma3000.h"
#include "HAL_Board.h"
#include "CTS_Layer.h"

static int8_t *accelData[ACCEL_AXES] = {
  &Cma3000_xAccel,
  &Cma3000_yAccel,
  &Cma3000_zAccel,  
};

void Measure_Init( void )
{
  Cma3000_init();
  TI_CAPT_Init_Baseline(&slider);
}

static uint8_t SliderRead( void )
{
  uint16_t sliderPosition = TI_CAPT_Slider(&slider);

  sliderPosition = (sliderPosition + 10) / 20;
  
  if (sliderPosition && sliderPosition < 6)
    return sliderPosition - 1;

  return 0xFF;
}

static volatile uint16_t timerMsbCounter;

#pragma vector = TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR(void)
{
  timerMsbCounter++;
  TA0CTL &= ~TAIFG;
}
static void TimerStart( void ){
  timerMsbCounter = 0;
  TA0CTL = TACLR;
  TA0CCR0 = 0xFFFF;
  TA0CTL = TASSEL_1 | ID_3 | MC__UP | TAIE;
}
static float TimerReadAndStop( void ){
  TA0CTL = 0;
  float time = TA0R;
  time += 65536. * timerMsbCounter;
  time /= 32768 / 8;
  return time;
}

extern void Tick( void );

#define LEDS_MASK (((1 << 5) - 1) << 3)

void Measure_Do(MeasureResult_TypeDef *result)
{
  Cma3000_readAccel();
  for (uint8_t i = 0; i< ACCEL_AXES; i++)
    result->accel[i] = -*accelData[i];
  
  uint8_t r = rand() % 5;
  Board_ledOff(LEDS_MASK);
  
  TimerStart();
  Board_ledOn( 1 << r << 3 );
  
  while (SliderRead() != r)
    Tick();
  result->time = TimerReadAndStop();
  
  Cma3000_readAccel();
  for (uint8_t i = 0; i< ACCEL_AXES; i++)
    result->accel[i] += *accelData[i];
}

