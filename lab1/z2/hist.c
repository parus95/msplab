#include <stdlib.h>
#include <math.h>
#include "hist.h"

void Hist_Init(Hist_TypeDef *hist, float start, float end, int intervals)
{
    hist->start = start;
    hist->end = end;
    hist->intervals = intervals;
    hist->hits = malloc(sizeof(float) * intervals);
    if (!hist->hits){
      while (1);
    }

    Hist_ResetHits(hist);
}

void Hist_ResetHits(Hist_TypeDef *hist)
{
  hist->sum = 0;
  hist->sumSqr = 0;
  hist->count = 0;
  for (int i=0; i< hist->intervals; i++)
    hist->hits[i] = 0;
}

int Hist_GetN(const Hist_TypeDef *hist, float value)
{
  if (value <= hist->start)
    return 0;
  if (value >= hist->end)
    return hist->intervals - 1;
  
  value -= hist->start;
  value /= (hist->end - hist->start);
  value *= (hist->intervals - 1);
  
  return (int)value;  
}

void Hist_Put(Hist_TypeDef *hist, float value)
{
  int intervalN = Hist_GetN(hist, value);
  hist->hits[intervalN]++;
  
  hist->sum += value;
  hist->sumSqr += value * value;
  hist->count++;
}

static float mean(float sum, int16_t n)
{
  if (n < 1)
    return NAN;
  return sum / n;
}
static float std(float sumSqr, float sum, int16_t n)
{
  if (n < 2)
    return NAN;
  return sqrt(sumSqr / (n - 1) - pow(sum, 2) / n / (n - 1) );
}

float Hist_Mean(const Hist_TypeDef *hist)
{
  return mean(hist->sum, hist->count);
}

float Hist_Std(const Hist_TypeDef *hist)
{
  return std(hist->sumSqr, hist->sum, hist->count);  
}
