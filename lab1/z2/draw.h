#pragma once
#include "hist.h"

void Draw_Init( void );
void Draw_Hist(const Hist_TypeDef *hist, const char *title);
void Draw_Data(float value);
