#include "draw.h"

void DrawHistTest( void ){
  float data[] = {1, 4, 8, 10, 2, 5, 10};
  Hist_TypeDef hist = { 
    1, 10,
    sizeof(data)/sizeof(data[0]),
    data
  };
  Draw_Hist(&hist, "Test hist");
  while(1);
}