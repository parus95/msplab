#pragma once
#include <stdint.h>

typedef struct {
  float start, end;
  int intervals;
  float *hits;
  
  int count;
  float sum, sumSqr;
} Hist_TypeDef;

void Hist_Init(Hist_TypeDef *hist, float start, float end, int intervals);

void Hist_ResetHits(Hist_TypeDef *hist);
void Hist_Put(Hist_TypeDef *hist, float value);

float Hist_Mean(const Hist_TypeDef *hist);
float Hist_Std(const Hist_TypeDef *hist);


//Hist_DeInit not realized