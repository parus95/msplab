#pragma once
#include <stdbool.h>

typedef struct {
  float x, y;
} Point_TypeDef;

typedef struct {
  Point_TypeDef point;
  float radius;
} Hole_TypeDef;

#define HOLES_MIN 3
#define HOLES_MAX 10
#define HOLE_RADIUS_MIN 3
#define HOLE_RADIUS_MAX 7

void Holes_Generate(Hole_TypeDef holes[], int *count, Point_TypeDef *ball);

bool PointDistanceIsSmaller(const Point_TypeDef *a, const Point_TypeDef *b, float limit);
