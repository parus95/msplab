
#include "msp430.h"

#include "HAL_Dogs102x6.h"
#include "HAL_Cma3000.h"
#include "HAL_Board.h"
#include "system.h"

#include <stdio.h>
#include <stdlib.h>
#include "Hole.h"


static void Limit(float *v, float min, float max)
{
  if (*v > max)
    *v = max;
  else if (*v < min)
    *v = min;
}

static void LimitPoint(Point_TypeDef *point)
{
  Limit(&point->x, 0, DOGS102x6_X_SIZE);
  Limit(&point->y, 0, DOGS102x6_Y_SIZE);
}

static inline void IIR(float *v, float n, float k)
{
  *v = k * (*v) + (1 - k) * n;
}

#define BALL_RADIUS 4
void Game ( void )
{
  
  Point_TypeDef point;// = {DOGS102x6_X_SIZE / 2, DOGS102x6_Y_SIZE / 2};
  Hole_TypeDef holes[HOLES_MAX];
  int holesCount;
  Holes_Generate(holes, &holesCount, &point);

  Point_TypeDef accel = {0, 0};
  bool done = false;
  do {
    rand();

    Cma3000_readAccel_offset();
    
    IIR(&accel.x, Cma3000_xAccel, 0.3);
    IIR(&accel.y, Cma3000_yAccel, 0.3);
    
    point.x -= accel.x / 10.;
    point.y += accel.y / 10.;
    LimitPoint(&point);
        
    Dogs102x6_clearScreen();
    
    for (int i = 0; i < holesCount; i++){
      Hole_TypeDef *hole = &holes[i];
      Dogs102x6_circleDraw((int)hole->point.x, (int)hole->point.y, (int)hole->radius, DOGS102x6_DRAW_NORMAL);
      if (!done && hole->radius >= BALL_RADIUS && PointDistanceIsSmaller(&hole->point, &point, hole->radius)){
        done = true;
      }
    }
    
    if (!done){
      Dogs102x6_circleDraw((int)point.x, (int)point.y, BALL_RADIUS, DOGS102x6_DRAW_NORMAL);
    }
    
    Dogs102x6_refresh(DOGS102x6_DRAW_ON_REFRESH);

    __delay_cycles(1000UL);
  } while (Cma3000_zAccel > -50);
}


int main( void )
{
  // Stop watchdog timer to prevent time out reset
  WDTCTL = WDTPW + WDTHOLD;

  SystemSetup();
  
  Dogs102x6_init();
  Dogs102x6_backlightInit();
  Dogs102x6_clearScreen();
  
  
  Cma3000_init();
  
  Cma3000_readAccel_offset();
  Cma3000_setAccel_offset(Cma3000_xAccel, Cma3000_yAccel, Cma3000_zAccel);

  Dogs102x6_setBacklight(3);
  
  while (1){
    Board_ledOn(LED1);
    Game();
    Board_ledOff(LED1);
    __delay_cycles(3 * 25000000UL);
  }
  
}
