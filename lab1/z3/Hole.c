#include "Hole.h"
#include <stdlib.h>
#include <math.h>
#include "HAL_Dogs102x6.h"

static int Random(int min, int max)
{
  return rand() % ( max - min + 1 ) + min;
}

static void Point_Generate( Point_TypeDef *point )
{
  point->x = rand() % DOGS102x6_X_SIZE;
  point->y = rand() % DOGS102x6_Y_SIZE;
}

static void Hole_Generate( Hole_TypeDef *hole )
{
  Point_Generate(&hole->point);
  hole->radius = Random( HOLE_RADIUS_MIN, HOLE_RADIUS_MAX );
}

void Holes_Generate(Hole_TypeDef holes[], int *count, Point_TypeDef *ball)
{
  *count = Random(HOLES_MIN, HOLES_MAX);
  for (int i = 0; i < *count; i++){
repeat:
    Hole_Generate(&holes[i]);
    for (int j = 0; j < i; j++){
      if (PointDistanceIsSmaller(&holes[i].point, &holes[j].point, holes[i].radius + holes[j].radius))
        goto repeat;
    }
  }
  
repeatBall:
  Point_Generate(ball);
  for (int j = 0; j < *count; j++)
    if (PointDistanceIsSmaller(&holes[j].point, ball, holes[j].radius * 2))
      goto repeatBall;
  
}
bool PointDistanceIsSmaller(const Point_TypeDef *a, const Point_TypeDef *b, float limit)
{
  float 
    dx = fabsf(a->x - b->x),
    dy = fabsf(a->y - b->y);
  if (dx >= limit || dy >= limit)
    return false;
  float delta = sqrt(dx * dx + dy * dy);
  return (delta < limit);
}