#include <stdlib.h>
#include <msp430.h>
#include "DynamicMemory.h"

#define SEMIFINISHEDS_COUNT_MAX 30
typedef struct {
  Pipeline_TypeDef pipeline;
  uint8_t allocatedSemifinisheds;
  Semifinished_TypeDef semifinisheds[SEMIFINISHEDS_COUNT_MAX];
}DynamicMemory_TypeDef;

static const DynamicMemory_TypeDef memInRom @ 0x1800;

static DynamicMemory_TypeDef memInRam;

void DynamicMemory_Restore( void )
{   
  FCTL3 = FWKEY;
  memInRam = memInRom;
  FCTL3 = FWKEY+LOCK;
}

static void Flash_Erase( const uint16_t addr )
{
  FCTL3 = FWKEY;                            // Clear Lock bit
  FCTL1 = FWKEY+ERASE;                      // Set Erase bit
  char * Flash_ptr = (char *) addr;
  *Flash_ptr = 0; 
  FCTL1 = FWKEY;
  FCTL3 = FWKEY+LOCK;
}

static void Flash_Write(const void *dest, const void *src, uint16_t sz){
  FCTL3 = FWKEY;
  FCTL1 = FWKEY+WRT;
  
  uint8_t *wPtr = (uint8_t *) dest;
  const uint8_t *rPtr = (uint8_t *) src;
  
  while(sz--){
    while (!(FCTL3 & WAIT)){}
    *(wPtr++) = *(rPtr++);
  }
  
  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY+LOCK;
}

void DynamicMemory_Save( void )
{
  Flash_Erase(0x1800 + 0 * 0x0080);
  Flash_Erase(0x1800 + 1 * 0x0080);
  Flash_Erase(0x1800 + 2 * 0x0080);
  Flash_Write(&memInRom, &memInRam, sizeof(DynamicMemory_TypeDef));
}

Pipeline_TypeDef *DynamicMemory_allocPipeline( void )
{
  return &memInRam.pipeline;
}

Semifinished_TypeDef *DynamicMemory_allocSemifinished( void ){
  return &memInRam.semifinisheds[memInRam.allocatedSemifinisheds++];
}

static const time_t savedTime @ 0x1980;

void DynamicMemory_SaveTime( void ){
  if (FCTL3 & LOCKA){
    FCTL3 = FWKEY + LOCKA;
  }
  Flash_Erase(0x1980);
  time_t time = GetSystemTime();
  Flash_Write(&savedTime, &time, sizeof(time_t));
  FCTL3 = FWKEY + LOCKA;
}

time_t DynamicMemory_GetSavedTime(){
  FCTL3 = FWKEY;
  time_t result = savedTime;
  FCTL3 = FWKEY+LOCK;
  return result;
}

Semifinished_TypeDef *DynamicMemory_GetSavedSemifinished( uint8_t index ){
  if (index >= memInRam.allocatedSemifinisheds)
    return NULL;
  return &memInRam.semifinisheds[index];
}
