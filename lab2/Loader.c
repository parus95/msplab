#include <stdlib.h>
#include "global.h"
#include "Loader.h"
#include "Machine.h"
#include "Storage.h"
#include "DynamicMemory.h"

#if LOADER_TRACE
#include <stdio.h>
#endif

int8_t GetNextStep(const Semifinished_TypeDef *s)
{
  if (s == NULL || s->nextStep >= s->totalSteps){
    return -1;
  }
  return s->steps[s->nextStep];
}

typedef struct {
  bool isBusy, isFree, isDone;
} MachineState;


typedef struct {
  const Semifinished_TypeDef *v;
  const Machine_TypeDef *machines;
} Context_TypeDef;

bool efun( const Semifinished_TypeDef *s, void *c)
{
  Context_TypeDef *context = (Context_TypeDef *) c;

  int8_t nextStep = GetNextStep(s);
  if (nextStep < 0 || !Machine_isFree(&context->machines[nextStep])) {
    return false;
  }else{
    context->v = s;
    return true;
  }
}

const Semifinished_TypeDef *FindForProceed( Pipeline_TypeDef *pipeline )
{
  Context_TypeDef context = { NULL, pipeline->machines };
  Storage_foreach(&context, efun);
  return context.v;
}

Semifinished_TypeDef *ExtractForProceed( Pipeline_TypeDef *pipeline )
{
  return Storage_extract(FindForProceed(pipeline));
}

void Loader_Tick(Pipeline_TypeDef *pipeline)
{
  MachineState aggregateMachinesState = {
    true, false, false
  };
  uint8_t doneMachineIndex;

  FOREACH_MACHINE_RW(pipeline)

    const MachineState state = {
      Machine_isBusy(machine),
      Machine_isFree(machine),
      Machine_isDone(machine)
    };
    aggregateMachinesState.isBusy &= state.isBusy;
    aggregateMachinesState.isFree |= state.isFree;
    aggregateMachinesState.isDone |= state.isDone;
    
    if (state.isDone) {
      doneMachineIndex = machineN;
    }
    
    if (!state.isBusy && machine->location == pipeline->loader.position)
    {
      if (state.isDone && pipeline->loader.obj == NULL) {
        pipeline->loader.obj = Machine_get(machine);
#if LOADER_TRACE
        printf("Pick %d from machine %d\r\n", pipeline->loader.obj->id, machineN);
#endif
        DynamicMemory_SaveTime();
        //Update direction
        const int8_t nextStep = GetNextStep(pipeline->loader.obj);
        if (nextStep >= 0 && Machine_isFree(&pipeline->machines[nextStep]))
        {
          // �� ��������� ����
          pipeline->loader.dir = pipeline->machines[nextStep].location > pipeline->loader.position ? 1 : -1;          
        } else {
          // �� �����
          pipeline->loader.dir = -1;          
        }
      } else if (state.isFree && GetNextStep(pipeline->loader.obj) == machineN) {
            Machine_put(machine, pipeline->loader.obj);
#if LOADER_TRACE
            printf("Push %d to machine %d\r\n", pipeline->loader.obj->id, machineN);
#endif
            DynamicMemory_SaveTime();
            pipeline->loader.obj = NULL;

            pipeline->loader.dir = 0;
      }
    }
  FOREACH_MACHINE_END

  if (pipeline->loader.position == 0) {
    if ( pipeline->loader.obj != NULL ){
      Storage_push(pipeline->loader.obj);
#if LOADER_TRACE
        printf("Push %d to storage\r\n", pipeline->loader.obj->id);
#endif
      pipeline->loader.obj = NULL;
      DynamicMemory_SaveTime();
    }
    pipeline->loader.obj = ExtractForProceed(pipeline);
    if (pipeline->loader.obj != NULL) {
      pipeline->loader.dir = 1;
#if LOADER_TRACE
        printf("Pick %d from storage\r\n", pipeline->loader.obj->id);
#endif
      DynamicMemory_SaveTime();
    } else {
      pipeline->loader.dir = 0;
    }
  }
  
  if (pipeline->loader.dir == 0){
    if (aggregateMachinesState.isDone) {
      pipeline->loader.dir = pipeline->machines[doneMachineIndex].location > pipeline->loader.position ? 1 : -1;
    } else {
      Context_TypeDef context = { NULL, pipeline->machines };
      Storage_foreach(&context, efun);
      if (context.v != NULL) {
        pipeline->loader.dir = -1; // �� �����
      }
    }
  }
  
  pipeline->loader.position += pipeline->loader.dir;
}