#pragma once

#include "Machine.h"

#define PIPELINE_MAX_MACHINES 10
#define PIPELINE_LOADER_DEFAULT {0, 0, NULL}


typedef struct {
  uint8_t position;
  int8_t dir;
  Semifinished_TypeDef *obj;
} Loader_TypeDef;

typedef struct {
  uint8_t machinesCount;
  Machine_TypeDef machines[PIPELINE_MAX_MACHINES];
  Loader_TypeDef loader;
} Pipeline_TypeDef;

