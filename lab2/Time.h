#pragma once
#include <stdint.h>
#include <stdbool.h>

typedef uint16_t time_t;

time_t GetSystemTime( void );

bool TimeGreater(time_t a, time_t b);

void IncrementSystemTime( void );

