#include "Time.h"

static volatile time_t systemTime = 0;

time_t GetSystemTime( void ) {
  return systemTime;
}

void IncrementSystemTime( void )
{
  systemTime++;
}

bool TimeGreater(time_t a, time_t b) {
  return a > b;
}