#include "LocalLCD.h"
#include "HAL_Dogs102x6.h"
#include "HAL_Board.h"
#include "LcdCommands.h"

extern void DrawInt(uint8_t x, uint8_t y, int value);
extern void DrawRect(int8_t x, int8_t y, int8_t w, int8_t h);

const uint8_t yC = DOGS102x6_Y_SIZE / 2;


static void LocalLCD_ProceedCommand(const uint8_t *buf, uint8_t size)
{
  switch (buf[0])
  {
  case LCDCMD_SETLED:
    LocalLCD_SetLed(buf[1]);
    break;
  case LCDCMD_CLEAR:
    LocalLCD_Clear();
    break;
  case LCDCMD_BACKGROUND:
    LocalLCD_Background();
    break;
  case LCDCMD_REFRESH:
    LocalLCD_Refresh();
    break;
  case LCDCMD_DRAWLOADER:
    LocalLCD_DrawLoader(buf[1]);
    break;
  case LCDCMD_DRAWSTORAGE:
    LocalLCD_DrawStorage(buf[1]);
    break;
  case LCDCMD_DRAWMACHINE:
    LocalLCD_DrawMachine(buf[1], buf[2], buf[3]);
    break;
  }
}

void LocalLCD_ProceedCommandByte(uint8_t b)
{
  static uint8_t state = 0;
  static uint8_t buf[5], size = 0, counter = 0;
  if (state == 0 && b == LCDCMD_SYNC)
  {
    state = 1;
  }
  else if (state == 1)
  {
    if (b <= 5)
    {
      state = 2;
      size = b;
      counter = 0;
    }
    else
    {
      state = 0;
    }
  }
  else if (state == 2)
  {
    buf[counter++] = b;
    if (counter >= size)
    {
      state = 0;
      LocalLCD_ProceedCommand(buf, size);
    }
  }
  else
  {
    state = 0;
  }
}

void LocalLCD_SetLed(bool state)
{
  if (state)
  {
    Board_ledOn(LED_ALL);
  }
  else
  {
    Board_ledOff(LED_ALL);
  }
}

void LocalLCD_Clear(void)
{
  Dogs102x6_clearScreen();
}

void LocalLCD_Background(void) {}

void LocalLCD_Refresh(void)
{
  Dogs102x6_refresh(DOGS102x6_DRAW_ON_REFRESH);
}

void LocalLCD_DrawLoader(uint8_t x)
{

  Dogs102x6_horizontalLineDraw(0, DOGS102x6_X_SIZE - 1, yC, DOGS102x6_DRAW_NORMAL);
  DrawRect(x - 2, yC - 2, 4, 4);
}

void LocalLCD_DrawStorage(uint8_t count)
{
  Dogs102x6_circleDraw(10, 10, 10, DOGS102x6_DRAW_NORMAL);
  DrawInt(5, 5, count);
}

void LocalLCD_DrawMachine(uint8_t x, bool upndown, int8_t unitId)
{
  const int8_t width = 15;
  const int8_t m = upndown ? 1 : -1;
  DrawRect((int8_t)x - width / 2, yC + m * 2, width, m * 20);
  if (unitId >= 0)
  {
    DrawInt(x /*  - width*/, yC + m * 10, unitId);
  }
}
