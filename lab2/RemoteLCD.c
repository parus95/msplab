#include "RemoteLCD.h"
#include "LcdCommands.h"
#if 0
void RemoteLCD_ProceedCommand(const uint8_t* cmd)
{
  LocalLCD_ProceedCommand(cmd);
}

void RemoteLCD_SetLed(bool state)
{
  LocalLCD_SetLed(state);
}

void RemoteLCD_Clear( void )
{
  LocalLCD_Clear( );
}

void RemoteLCD_Background( void )
{
  LocalLCD_Background( );
}

void RemoteLCD_Refresh( void )
{
  LocalLCD_Refresh(  );
}

void RemoteLCD_DrawLoader(uint8_t x)
{
  LocalLCD_DrawLoader(x);
}

void RemoteLCD_DrawStorage(uint8_t count)
{
  LocalLCD_DrawStorage(count);
}

void RemoteLCD_DrawMachine(uint8_t x, bool upndown, int8_t unitId)
{
  LocalLCD_DrawMachine(x, upndown, unitId);
}
#else

#define BC const uint8_t cmdbuf[] =
#define SC RemoteLCD_SendCommand(cmdbuf, sizeof(cmdbuf))

void RemoteLCD_SetLed(bool state)
{
  BC{LCDCMD_SETLED, state};
  SC;
}

void RemoteLCD_Clear(void)
{
  BC{LCDCMD_CLEAR};
  SC;
}

void RemoteLCD_Background(void)
{
  BC{LCDCMD_BACKGROUND};
  SC;
}

void RemoteLCD_Refresh(void)
{
  BC{LCDCMD_REFRESH};
  SC;
}

void RemoteLCD_DrawLoader(uint8_t x)
{
  BC{LCDCMD_DRAWLOADER, x};
  SC;
}

void RemoteLCD_DrawStorage(uint8_t count)
{
  BC{LCDCMD_DRAWSTORAGE, count};
  SC;
}

void RemoteLCD_DrawMachine(uint8_t x, bool upndown, int8_t unitId)
{
  BC{LCDCMD_DRAWMACHINE, x, upndown, unitId};
  SC;
}

#endif