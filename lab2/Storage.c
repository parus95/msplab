#include <stdlib.h> // NULL
#include <string.h> // memcpy
#include "Storage.h"

struct StorageNode_Struct{
  StorageElement_TypeDef *obj;
  struct StorageNode_Struct *next;
};

typedef struct StorageNode_Struct StorageNode_TypeDef;

static StorageNode_TypeDef storage = { NULL, NULL };

static StorageNode_TypeDef* mallocNode( void )
{
  return (StorageNode_TypeDef*)malloc(sizeof(StorageNode_TypeDef));
}
static void freeNode( StorageNode_TypeDef *node )
{
  free(node);
}

void Storage_push(StorageElement_TypeDef *obj) {
  const StorageNode_TypeDef newNode = {
    obj,
    NULL
  };
  StorageNode_TypeDef *lastNode = &storage;
  while (lastNode->next != NULL){
    lastNode = lastNode->next;
  }
  StorageNode_TypeDef *mem = mallocNode();
  *mem = newNode;
  lastNode->next = mem;
}

StorageElement_TypeDef * Storage_pop( void ) {
  StorageNode_TypeDef *firstNode = storage.next;
  if (firstNode != NULL) {
    storage.next = firstNode->next;
    StorageElement_TypeDef *result = firstNode->obj;
    freeNode(firstNode);
    return result;
  } else {
    return NULL;
  }
}

bool Storage_any( void ){
  return (NULL != storage.next);
}

int Storage_count( void ){
  int count = 0;
  StorageNode_TypeDef *node = storage.next;
  while (node != NULL) {
    node = node->next;
    count++;
  }
  return count;
}

void Storage_foreach( void *context, bool (*func)(const StorageElement_TypeDef *el, void *context) )
{
  StorageNode_TypeDef *node = storage.next;
  while (node != NULL) {
    if (func(node->obj, context)){
      break;
    }
    node = node->next;
  }
}
StorageElement_TypeDef * Storage_extract( const StorageElement_TypeDef *obj )
{
  if (obj == NULL){
    return NULL;
  }
  StorageNode_TypeDef *node = &storage;
  while (node->next){
    if (node->next->obj == obj){
      StorageNode_TypeDef *middleNode = node->next;
      node->next = middleNode->next;
      StorageElement_TypeDef *result = middleNode->obj;
      freeNode(middleNode);
      return result;
    }
    node = node->next;
  }
  return NULL;
}