#include <stdio.h>
#include "Tests.h"
#include "Pipeline.h"
#include "Loader.h"
#include "Machine.h"


static void Test_Logic( void )
{
  printf("Logic test started\r\n");

  Pipeline_TypeDef pipeline;
  GenerateTestData(&pipeline);

  uint16_t cnt = 10000;
  while (cnt--){
    Loader_Tick(&pipeline);
    IncrementSystemTime();
  }
  
  printf("Logic test finished\r\n");
}

void Tests_Run( void )
{
  Test_Logic();
  while (true);
}
