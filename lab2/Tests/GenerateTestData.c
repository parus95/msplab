#include "Tests.h"
#include "Storage.h"
#include "DynamicMemory.h"

Semifinished_TypeDef *Semifinished_Clone(const Semifinished_TypeDef *el)
{
  Semifinished_TypeDef *result = DynamicMemory_allocSemifinished();
  *result = *el;
  return result;
}

static void PushToStorage(Semifinished_TypeDef *el, uint8_t count)
{
  while(count--){
    Storage_push(Semifinished_Clone(el));
    el->id++;
  }
}

void GenerateTestData(Pipeline_TypeDef *pipeline){
  Semifinished_TypeDef sem = {1, 0, 3, { 0, 2, 1}};
  PushToStorage(&sem, 3);  

  Semifinished_TypeDef sem2 = {sem.id, 0, 4, { 1, 0, 3, 2}};
  PushToStorage(&sem2, 4);
  
  Pipeline_TypeDef newPipeline = {
    4,
    {
      MACHINE_NEW(5,  10, true),
      MACHINE_NEW(10, 30, true),
      MACHINE_NEW(20, 60, true),
      MACHINE_NEW(2,  50, false),
    },
    PIPELINE_LOADER_DEFAULT
  };
  *pipeline = newPipeline;
}
