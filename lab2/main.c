#include <stdio.h>
#include <stdlib.h>
#include <msp430.h>
#include <string.h>

#include "HAL_Dogs102x6.h"
#include "HAL_Cma3000.h"
#include "HAL_Board.h"
#include "HAL_Wheel.h"
#include "HAL_Menu.h"
#include "HAL_Buttons.h"
#include "CTS_Layer.h"
#include "HAL_AppUart.h"
#include "system.h"
#include "RemoteLCD.h"
#include "LocalLCD.h"
#include "LcdCommands.h"

#include "Pipeline.h"
#include "Storage.h"

#include "DynamicMemory.h"
#include "Tests.h"
#include "Loader.h"

#define MCLK 25000000
#define TICKSPERMS (MCLK / 1000)

static char *itoa(int i)
{
  static char line[5];
  snprintf(line, 5, "%d", i);
  return line;
}

static inline bool AppUart_TryGetChar(uint8_t *r)
{
  if (!(UCA1IFG & UCRXIFG))
  {
    return false;
  }
  else
  {
    *r = UCA1RXBUF;
    return true;
  }
}

static void TryP(void)
{
  uint8_t r;
  if (AppUart_TryGetChar(&r))
  {
    LocalLCD_ProceedCommandByte(r);
  }
}

static inline void RemoteLCD_PutByte(uint8_t txChar)
{
  while (!(UCA1IFG & UCTXIFG))
  {
    TryP();
  }
  UCA1TXBUF = txChar;
  TryP();
}
void RemoteLCD_SendCommand(const uint8_t *cmd, uint8_t len)
{
  RemoteLCD_PutByte(LCDCMD_SYNC);
  RemoteLCD_PutByte(len);
  while (len--)
  {
    RemoteLCD_PutByte(*(cmd++));
  }
}

void DrawInt(uint8_t x, uint8_t y, int value)
{
  Dogs102x6_stringDrawXY(x, y, itoa(value), DOGS102x6_DRAW_NORMAL);
}
void DrawRect(int8_t x, int8_t y, int8_t w, int8_t h)
{
  const uint8_t style = DOGS102x6_DRAW_NORMAL;
  if (x < 0)
    x = 0;
  if (y < 0)
    y = 0;
  int8_t xpw = x + w;
  if (xpw < 0)
    xpw = 0;
  int8_t yph = y + h;
  if (yph < 0)
    yph = 0;

  Dogs102x6_lineDraw(x, y, xpw, y, style);
  Dogs102x6_lineDraw(x, y, x, yph, style);
  Dogs102x6_lineDraw(xpw, y, xpw, yph, style);
  Dogs102x6_lineDraw(x, yph, xpw, yph, style);
}

void DrawPipeline(const Pipeline_TypeDef *pipeline, bool drawIndexes)
{
  RemoteLCD_Clear();
  RemoteLCD_DrawStorage(Storage_count());

  RemoteLCD_DrawLoader(pipeline->loader.position);

  FOREACH_MACHINE(pipeline)

  int8_t id = -1;
  if (drawIndexes || !Machine_isFree(machine))
  {
    id = drawIndexes ? machineN : machine->obj->id;
  }

  RemoteLCD_DrawMachine(machine->location, machine->upndown, id);

  FOREACH_MACHINE_END
}

static inline uint8_t GetSliderPosition(void)
{
  uint16_t sliderPosition = TI_CAPT_Slider(&slider);

  sliderPosition = (sliderPosition + 10) / 20;

  if (sliderPosition && sliderPosition < 6)
    return sliderPosition - 1;

  return 0xFF;
}

static uint8_t SliderRead(void)
{
  const uint8_t result = GetSliderPosition();
  while (GetSliderPosition() != 0xFF)
  {
  }
  return result;
}

static void SetupPipeline(Pipeline_TypeDef *pipeline)
{
  while (pipeline->machinesCount < PIPELINE_MAX_MACHINES)
  {
    Cma3000_readAccel_offset();

    Machine_TypeDef *machine = &pipeline->machines[pipeline->machinesCount];
    if (machine->processingDuration == 0)
      machine->processingDuration = 1;
    if (machine->location == 0)
      machine->location = 1;

    const int8_t udaccel = Cma3000_yAccel;
    if (abs(udaccel) > 10)
    {
      machine->upndown = udaccel > 0;
    }

    const int8_t locAccel = -Cma3000_xAccel;
    if (abs(locAccel) > 10)
    {
      uint8_t loc = machine->location;
      if (locAccel > 0)
        loc++;
      else
        loc--;
      if (loc != 0 && loc != 0xFF)
      {
        machine->location = loc;
      }
    }

    pipeline->machinesCount++;
    DrawPipeline(pipeline, true);
    pipeline->machinesCount--;
    DrawInt(DOGS102x6_X_SIZE - 40, DOGS102x6_Y_SIZE - 9, machine->processingDuration);
    Dogs102x6_refresh(DOGS102x6_DRAW_ON_REFRESH);

    const uint8_t slider = SliderRead();
    if (slider == 4)
    {
      break;
    }
    else
      switch (slider)
      {
      case 0:
        pipeline->machinesCount++;
        break;

      case 1:
        if (pipeline->machinesCount > 0)
        {
          pipeline->machinesCount--;
        }
        break;

      case 2:
        if (machine->processingDuration > 0)
        {
          machine->processingDuration--;
        }
        break;
      case 3:
        if (machine->processingDuration < 255)
        {
          machine->processingDuration++;
        }
        break;
      }

    __delay_cycles(50UL * TICKSPERMS);
  }
}

static void SlaveMode(void)
{
  while (true)
  {
    TryP();
  }
}

void SetupSemifinisheds(uint8_t machinesCount)
{
  if (machinesCount == 0)
    return;

  while (~PAIN & BIT7)
  {
  }

  Semifinished_TypeDef prototype = {1, 0, 0, {0}};
  while (PAIN & BIT7)
  {
    char line[20] = "";
    strcat(line, itoa(prototype.id));
    strcat(line, " -");

    for (uint8_t stepN = 0; stepN < prototype.totalSteps; stepN++)
    {
      strcat(line, " ");
      strcat(line, itoa(prototype.steps[stepN]));
    }

    Dogs102x6_clearScreen();
    Dogs102x6_stringDraw(2, 0, line, DOGS102x6_DRAW_NORMAL);
    Dogs102x6_refresh(DOGS102x6_DRAW_ON_REFRESH);

    uint8_t slider = SliderRead();
    switch (slider)
    {
    case 0:
      if (prototype.totalSteps > 0)
        prototype.totalSteps--;
      break;
    case 1:
      if (prototype.totalSteps > 0 && prototype.steps[prototype.totalSteps - 1] > 0)
        prototype.steps[prototype.totalSteps - 1]--;
      break;
    case 2:
      if (prototype.totalSteps > 0 && prototype.steps[prototype.totalSteps - 1] < machinesCount - 1)
        prototype.steps[prototype.totalSteps - 1]++;
      break;
    case 3:
      if (prototype.totalSteps < SEMIFINISHED_MAXSTEPS)
      {
        prototype.totalSteps++;
      }
      break;
    case 4:
      Storage_push(Semifinished_Clone(&prototype));
      prototype.id++;
      break;
    }
  }
}

Pipeline_TypeDef *MainMenu(void)
{
  static const char *const mainMenu[] = {
      "=Pipeline emulator=",
      "1. Load example ",
      "2. Create new ",
      "3. Load from mem ",
      "4. Slave mode",
  };
  Dogs102x6_stringDraw(7, 0, "*S1=Enter S2=Esc*", DOGS102x6_DRAW_NORMAL);
  Pipeline_TypeDef *const pipeline = DynamicMemory_allocPipeline();
  switch (Menu_active((char **)mainMenu, 4))
  {
  case 1:
    GenerateTestData(pipeline);
    DynamicMemory_Save();
    return pipeline;

  case 2:
    SetupPipeline(pipeline);
    SetupSemifinisheds(pipeline->machinesCount);
    DynamicMemory_Save();
    return pipeline;

  case 3:
    DynamicMemory_Restore();
    {
      time_t savedTime = DynamicMemory_GetSavedTime();

      uint8_t i = 0;
      while (true)
      {
        Semifinished_TypeDef *semifinished = DynamicMemory_GetSavedSemifinished(i++);
        if (semifinished != NULL)
        {
          Storage_push(semifinished);
        }
        else
        {
          break;
        }
      }

      while (savedTime--)
      {
        Loader_Tick(pipeline);
        IncrementSystemTime();
      }
    }
    return pipeline;

  case 4:
    SlaveMode();
    break;

  default:
    return NULL;
  }
  return NULL;
}
int main(void)
{
  WDTCTL = WDTPW + WDTHOLD;

  //printf("Hello, world!");
  //Tests_Run();
  SystemSetup();

  Dogs102x6_init();
  Dogs102x6_backlightInit();
  Dogs102x6_clearScreen();

  TI_CAPT_Init_Baseline(&slider);

  Cma3000_init();
  __delay_cycles(1000UL * TICKSPERMS);
  Cma3000_readAccel_offset();
  Cma3000_setAccel_offset(Cma3000_xAccel, Cma3000_yAccel, Cma3000_zAccel);

  Dogs102x6_setBacklight(3);
  Buttons_init(BUTTON_ALL);
  Buttons_interruptEnable(BUTTON_S1);
  Wheel_init();

  __enable_interrupt();

  Wheel_enable();

  // Workaround
  const uint8_t p4sel_original = P4SEL;
  AppUart_init();
  P4SEL |= p4sel_original;

  Pipeline_TypeDef *const pipeline = MainMenu();

  while (true)
  {

    Loader_Tick(pipeline);
    DrawPipeline(pipeline, false);
    RemoteLCD_Refresh();

    RemoteLCD_SetLed(pipeline->loader.obj == NULL);

    if (/*GetSystemTime() == (550 - 90 - 3) ||*/ pipeline->loader.position > 90)
    {
      while (1)
        ;
    }
    IncrementSystemTime();
    for (uint8_t i = 50; i; i--)
    {
      TryP();
      __delay_cycles(TICKSPERMS);
    }
  }
}