#pragma once
#include <stdbool.h>
#include <stdlib.h> // NULL
#include "global.h"

#define MACHINE_NEW(duration, location, upnDown) { duration, 0, NULL, location, upnDown }

typedef struct {
  uint8_t processingDuration;
  time_t endTime;
  Semifinished_TypeDef *obj;
  
  uint8_t location; // ��� ����������
  bool upndown; // ��� ���������
} Machine_TypeDef;

bool Machine_put( Machine_TypeDef *machine, Semifinished_TypeDef *obj );
bool Machine_isFree( const Machine_TypeDef *machine );
bool Machine_isDone( const Machine_TypeDef *machine );
bool Machine_isBusy( const Machine_TypeDef *machine );
Semifinished_TypeDef *Machine_get( Machine_TypeDef *machine );

