#pragma once
#include <stdint.h>
#include <stdbool.h>
#include "Time.h"
#define SEMIFINISHED_MAXSTEPS 5


typedef struct {
  uint8_t id, nextStep, totalSteps;
  uint8_t steps[SEMIFINISHED_MAXSTEPS];  
} Semifinished_TypeDef;


Semifinished_TypeDef *Semifinished_Clone(const Semifinished_TypeDef *el);

#define FOREACH_MACHINE(pipeline) \
for (uint8_t machineN = 0; machineN < pipeline->machinesCount; machineN++) \
  { \
    const Machine_TypeDef * const machine = &pipeline->machines[machineN];
#define FOREACH_MACHINE_RW(pipeline) \
for (uint8_t machineN = 0; machineN < pipeline->machinesCount; machineN++) \
  { \
    Machine_TypeDef * const machine = &pipeline->machines[machineN];
#define FOREACH_MACHINE_END }

