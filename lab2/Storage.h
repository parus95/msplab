#pragma once
#include <stdbool.h>
#include "global.h"

typedef Semifinished_TypeDef StorageElement_TypeDef;

void Storage_push(StorageElement_TypeDef *obj);
StorageElement_TypeDef * Storage_pop( void );
bool Storage_any( void );
int Storage_count( void );
void Storage_foreach( void *context, bool (*func)(const StorageElement_TypeDef *el, void *context) );
StorageElement_TypeDef * Storage_extract( const StorageElement_TypeDef *obj );
