#include <stdlib.h> // NULL
#include "Machine.h"

bool Machine_put( Machine_TypeDef *machine, Semifinished_TypeDef *obj )
{
  if (machine->obj != NULL){
    return false;
  }
  machine->obj = obj;
  machine->endTime = GetSystemTime() + machine->processingDuration;
  return true;
}

bool Machine_isFree( const Machine_TypeDef *machine )
{
  return machine->obj == NULL;
}

bool Machine_isDone( const Machine_TypeDef *machine )
{
  return machine->obj != NULL && TimeGreater(GetSystemTime(), machine->endTime);
}
bool Machine_isBusy( const Machine_TypeDef *machine )
{
  return machine->obj != NULL && !TimeGreater(GetSystemTime(), machine->endTime);
}

Semifinished_TypeDef *Machine_get( Machine_TypeDef *machine )
{
  if (Machine_isDone(machine))
  {
    Semifinished_TypeDef *result = machine->obj;
    machine->obj = NULL;
    result->nextStep++;
    return result;
  }else {
    return NULL;
  }  
}
