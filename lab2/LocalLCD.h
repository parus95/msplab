#pragma once
#include <stdint.h>
#include <stdbool.h>

void LocalLCD_ProceedCommandByte(uint8_t b);
void LocalLCD_SetLed(bool state);
void LocalLCD_Clear( void );
void LocalLCD_Background( void );
void LocalLCD_Refresh( void );
void LocalLCD_DrawLoader(uint8_t x);
void LocalLCD_DrawStorage(uint8_t count);
void LocalLCD_DrawMachine(uint8_t x, bool upndown, int8_t unitId);
