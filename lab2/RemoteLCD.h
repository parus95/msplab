#pragma once
#include <stdbool.h>
#include <stdint.h>

extern void RemoteLCD_SendCommand(const uint8_t* cmd, uint8_t len);
void RemoteLCD_SetLed(bool state);
void RemoteLCD_Clear( void );
void RemoteLCD_Background( void );
void RemoteLCD_Refresh( void );
void RemoteLCD_DrawLoader(uint8_t x);
void RemoteLCD_DrawStorage(uint8_t count);
void RemoteLCD_DrawMachine(uint8_t x, bool upndown, int8_t unitId);
