#pragma once
#include "global.h"
#include "Pipeline.h"

void DynamicMemory_Restore( void );
void DynamicMemory_Save( void );
void DynamicMemory_SaveTime( void );

time_t DynamicMemory_GetSavedTime();
Semifinished_TypeDef *DynamicMemory_GetSavedSemifinished( uint8_t index );

Pipeline_TypeDef *DynamicMemory_allocPipeline( void );
Semifinished_TypeDef *DynamicMemory_allocSemifinished( void );
