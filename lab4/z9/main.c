//----------------------------------------------------------------------------------
// Project: Blink MSP430 using BIOS Mailbox/Queue (SOLUTION)
// Author: Eric Wilbur
// Date: May 2015
//
// Follow these steps to create this project in CCSv6.0:
// 1. Project -> New CCS Project
// 2. Select Template:
//    - TI-RTOS for MSP430 -> Driver Examples -> 5529 LP -> Example Projects ->
//      Empty Project
//    - Empty Project contains full instrumentation (UIA, RTOS Analyzer) and
//      paths set up for the TI-RTOS version of MSP430Ware
// 3. Delete the following files:
//    - Board.h, empty.c, MSP_EXP4305529LP.c/h, empty_readme.txt
// 4. Add main.c from TI-RTOS Workshop Solution file for this lab
// 5. Edit empty.cfg as needed (to add/subtract) BIOS services, delete Task
// 6. Build, load, run...
//
// FYI - Part B solution for Queues is actually shown working. Part A solution
// (Mailbox) is populated below but commented out.
//----------------------------------------------------------------------------------


//----------------------------------------
// BIOS header files
//----------------------------------------
#include <xdc/std.h>  						//mandatory - have to include first, for BIOS types
#include <ti/sysbios/BIOS.h> 				//mandatory - if you call APIs like BIOS_start()
#include <xdc/runtime/Log.h>				//needed for any Log_info() call
#include <xdc/cfg/global.h> 				//header file for statically defined objects/handles


//-----------------------------------------
// MSP430 Header Files
//-----------------------------------------
#include <driverlib.h>


//-----------------------------------------
// MSP430 MCLK frequency settings
// Used to set MCLK frequency
// Ratio = MCLK/FLLREF = 8192KHz/32KHz
//-----------------------------------------
#define MCLK_DESIRED_FREQUENCY_IN_KHZ  8000                            // 8MHz
#define MCLK_FLLREF_RATIO              MCLK_DESIRED_FREQUENCY_IN_KHZ / ( UCS_REFOCLK_FREQUENCY / 1024 )    // Ratio = 250

#define GPIO_ALL	GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3| \
					GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7


//-----------------------------------------
// Prototypes
//-----------------------------------------
void hardware_init(void);
void ledToggle(void);
void Timer_ISR(void);


//-----------------------------------------
// Globals
//-----------------------------------------
volatile int16_t i16ToggleCount = 0;

// Timer A1 parameter structure used for Timer A1 UP Mode (interrupt source)
Timer_A_initUpModeParam initUpParam =
{   TIMER_A_CLOCKSOURCE_ACLK,                 	// Use ACLK (slower clock)
    TIMER_A_CLOCKSOURCE_DIVIDER_1,          	// Input clock = ACLK / 1 = 32KHz
    0x4000,                                    	// Period (0xFFFF/4):  4000 / 32Khz = 1/2 second
    TIMER_A_TAIE_INTERRUPT_DISABLE,    			// Enable TAR -> 0 interrupt
    TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE,       	// Enable CCR0 compare interrupt
    TIMER_A_DO_CLEAR,                        	// Clear TAR & clock divider
    1											// start timer immediately
};


//------------------------
// for Mailbox - Part A
//------------------------
//typedef struct MsgObj {
//	Int	val;            		// message value
//} MsgObj, *Msg;


//------------------------
// for Queue - Part B
//------------------------
typedef struct MsgObj {
	Queue_Elem	elem;
	Int	val;            		// message value
} MsgObj, *Msg;					// Use Msg as pointer to MsgObj




//---------------------------------------------------------------------------
// main()
//---------------------------------------------------------------------------
void main(void)
{

   hardware_init();				// init hardware via Xware

   BIOS_start();				// start BIOS Scheduler

}


//-----------------------------------------------------------------------------
// hardware_init()
//-----------------------------------------------------------------------------
void hardware_init(void)					//called by main
{
	// Disable the Watchdog Timer (important, as this is enabled by default)
	WDT_A_hold( WDT_A_BASE );


    // Set MCLK frequency to 8192 KHz
	// First, set DCO FLLREF to 32KHz = REF0
	UCS_initClockSignal(UCS_FLLREF, UCS_REFOCLK_SELECT, UCS_CLOCK_DIVIDER_1);

	// Second, Set Ratio and Desired MCLK Frequency = 8192KHz and initialize DCO
	UCS_initFLLSettle(MCLK_DESIRED_FREQUENCY_IN_KHZ, MCLK_FLLREF_RATIO);

	// Set GPIO ports to low-level outputs
    GPIO_setAsOutputPin( GPIO_PORT_P1, GPIO_ALL );
    GPIO_setAsOutputPin( GPIO_PORT_P2, GPIO_ALL );
    GPIO_setAsOutputPin( GPIO_PORT_P3, GPIO_ALL );
    GPIO_setAsOutputPin( GPIO_PORT_P4, GPIO_ALL );
    GPIO_setAsOutputPin( GPIO_PORT_P5, GPIO_ALL );
    GPIO_setAsOutputPin( GPIO_PORT_P6, GPIO_ALL );
    GPIO_setAsOutputPin( GPIO_PORT_PJ, GPIO_ALL );

    GPIO_setOutputLowOnPin( GPIO_PORT_P1, GPIO_ALL );
    GPIO_setOutputLowOnPin( GPIO_PORT_P2, GPIO_ALL );
    GPIO_setOutputLowOnPin( GPIO_PORT_P3, GPIO_ALL );
    GPIO_setOutputLowOnPin( GPIO_PORT_P4, GPIO_ALL );
    GPIO_setOutputLowOnPin( GPIO_PORT_P5, GPIO_ALL );
    GPIO_setOutputLowOnPin( GPIO_PORT_P6, GPIO_ALL );
    GPIO_setOutputLowOnPin( GPIO_PORT_PJ, GPIO_ALL );


    // Configure I/O's that we're using (redundant, based on the previous settings)
    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0 );
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0 );


    // INIT TIMER A1 in UP MODE (BIOS uses Timer A0, so we want to use A1)

    Timer_A_initUpMode( TIMER_A1_BASE, &initUpParam );              	// Set up Timer A1

    // Clear/enable flags and start timer
    Timer_A_clearCaptureCompareInterrupt( TIMER_A1_BASE,
            TIMER_A_CAPTURECOMPARE_REGISTER_0 );                        // Clear CCR0IFG

}



//---------------------------------------------------------------------------
// mailbox_queue Task() - Run by BIOS_Start(), then unblocked by Timer ISR
//
// Places state of LED (msg.val) into a mailbox for ledToggle() to use
//---------------------------------------------------------------------------
void mailbox_queue(void)
{

//---------------------------------
// msg used for Mailbox and Queue
//---------------------------------
	MsgObj msg;													// create an instance of MsgObj named msg

//---------------------------------
// msgp used for Queue only
//---------------------------------
	Msg msgp;													// Queues pass POINTERS, so we need a pointer of type Msg
	msgp = &msg;												// init message pointer to address of msg


	msg.val = 1;												// set initial value of msg.val (LED state)

	while(1){

		msg.val ^= 1;											// toggle msg.val (LED state)

		Semaphore_pend(mailbox_queue_Sem, BIOS_WAIT_FOREVER);	// wait on semaphore from Timer ISR

//------------------------------
// MAILBOX CODE follows...
//------------------------------
//		Mailbox_post (LED_Mbx, &msg, BIOS_WAIT_FOREVER);		// post msg containing LED state into the MAILBOX


//------------------------------
// QUEUE CODE follows...
//------------------------------
		Queue_put(LED_Queue, (Queue_Elem*)msgp);				// pass pointer to Message object via LED_Queue
		Semaphore_post (QueSem);								// unblock Queue_get to get msg

	}

}



//---------------------------------------------------------------------------
// ledToggle()  - called by BIOS_Start(), then unblocked by mailbox_queue()
//
// Toggle LED via GPIO pin
// Add "+ GPIO_PIN1 to GPIO_setOutput call to add blue X LED
//---------------------------------------------------------------------------
void ledToggle(void)
{

//---------------------------------
// msg used for Mailbox and Queue
//---------------------------------
	MsgObj msg;													//define msg using MsgObj struct created earlier

//---------------------------------
// msgp used for Queue only
//---------------------------------
	Msg msgp;													//define pointer to MsgObj to use with queue put/get
	msgp = &msg;												//init msgp to point to address of msg (used for put/get)


	while(1)
	{

//------------------------------
// MAILBOX CODE follows...
//------------------------------
//		Mailbox_pend(LED_Mbx, &msg, BIOS_WAIT_FOREVER);			// wait/block until post of msg, get msg.val


//------------------------------
// QUEUE CODE follows...
//------------------------------
		Semaphore_pend(QueSem, BIOS_WAIT_FOREVER);				// unblocked by mailbox_queue() when Queue has msg
		msgp = Queue_get(LED_Queue);							// read contents of queue to get value of LED state


//		if (msg.val)											// MAILBOX "if" - msg.val contains LED state

		if(msgp->val)											// QUEUE "if" - mspg->val contains LED state for QUEUE's the use pointers

		{
			GPIO_setOutputHighOnPin( GPIO_PORT_P1, GPIO_PIN0 );	// turn LED ON
		}
		else
		{
			GPIO_setOutputLowOnPin( GPIO_PORT_P1, GPIO_PIN0 );	// turn LED OFF
		}

		i16ToggleCount += 1;									// keep track of #toggles

		Log_info1("LED TOGGLED [%u] TIMES", i16ToggleCount);

	}
}



//---------------------------------------------------------------------------
// Timer ISR - called by BIOS Hwi (see app.cfg)
//
// Posts Sem to unblock mailbox/queue Task
//---------------------------------------------------------------------------
void Timer_ISR(void)
{
	Semaphore_post(mailbox_queue_Sem);							// post sem to unblock mailbox-queue Task
}



