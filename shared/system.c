#include "system.h"

#include "msp430.h"

#include "HAL_PMM.h"
#include "HAL_UCS.h"
#include "HAL_Board.h"

void SystemSetup( void )
{
  Board_init();

  // Set Vcore to accomodate for max. allowed system speed
  SetVCore(3);

  // Use 32.768kHz XTAL as reference
  LFXT_Start(XT1DRIVE_0);

  // Set system clock to max (25MHz)
  Init_FLL_Settle(25000, 762);

  SFRIFG1 = 0;
  SFRIE1 |= OFIE;
}