#include <msp430.h>
#include <stdint.h>

#include "Generator.h"


//TB0.3 - P7.5

void Generator_Init( void ){
  TB0CTL = MC__UP | ID__4 | TBSSEL__SMCLK;
  
  TB0CCR0 = 65535;
  
  TB0CCTL4 = OUTMOD_7;
  TB0CCR4 = 4095;
  
  TB0CCTL3 = OUTMOD_7;
  
  P7DIR |= BIT5;
  P7SEL |= BIT5;
  P7OUT |= BIT5;
}


void Generator_Setup(float period, float k){
  const uint16_t p = TB0CCR0 =(uint16_t)(period * 25000. / 4);
  TB0CCR3 = (uint16_t)(p * k);
  TB0CCR4 = p >> 2;
}

