#include <math.h>
#include <stdbool.h>
#include "system.h"
#include "msp430.h"

#include "display.h"
#include "Generator.h"
#include "Receiver.h"
#include "Input.h"

static float period, k;
static uint8_t prevMode = 0;

static float wheelOffset = 0;

const int PERIOD_MAX = 8;
void Configuration_Tick()
{
  float wheel = Input_getWheel();
  
  uint8_t mode = Input_getMode();
  if (mode != prevMode){
    prevMode = mode;
    wheelOffset = wheel;
  }

  if (mode > 1){
    return;
  }
  
  wheel -= wheelOffset;
  
  if (fabs(wheel) > 0.05){
    wheelOffset += wheel;
    if (mode == 0){
      period += wheel * PERIOD_MAX;
      if (period < 0 ) period = 0;
      if (period > PERIOD_MAX ) period = PERIOD_MAX;
    }else{
      k += wheel * 1;
      if (k < 0) k = 0;
      if (k > 1) k = 0;
    }
    
    Generator_Setup(period, k);
    Display(Display_Generator, period, k);
  }
}

void Receiver_Callback( float mPeriod, float mK )
{
  Display(Display_Receiver, mPeriod, mK);
}

int main( void )
{
  // Stop watchdog timer to prevent time out reset
  WDTCTL = WDTPW + WDTHOLD;

  SystemSetup();

  Display_Init();
  Generator_Init();
  Receiver_Init();
  Input_Init();
  
  __enable_interrupt();
  
  while (true){
    Input_Tick();
    Receiver_Tick();
    
    Configuration_Tick();
  }
}
