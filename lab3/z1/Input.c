#include "Input.h"
//#include "CTS_HAL.h"
//#include "CTS_Layer.h"
#include "HAL_Buttons.h"
#include "HAL_Board.h"
#include "HAL_Wheel.h"

static uint8_t mode = 2;

static void DisplayMode( void ){
  Board_ledOff(LED_ALL);
  Board_ledOn(1 << (mode + 3));
}

//static inline uint8_t GetSliderPosition(void)
//{
//  uint16_t sliderPosition = TI_CAPT_Slider(&slider);
//
//  sliderPosition = (sliderPosition + 10) / 20;
//
//  if (sliderPosition && sliderPosition < 6)
//    return sliderPosition - 1;
//
//  return 0xFF;
//}

void Input_Init( void ){
  //TI_CAPT_Init_Baseline(&slider);
  Buttons_init(BUTTON_ALL);
  Buttons_interruptEnable(BUTTON_ALL);
  
  Wheel_init();
  Wheel_enable();

  DisplayMode();
}

void Input_Tick( void ){
  switch (buttonsPressed){
    case BUTTON_S1: mode = 0; DisplayMode(); break;
    case BUTTON_S2: mode = 1; DisplayMode(); break;
  }
  //const uint8_t position = GetSliderPosition();
  //if (position < 5){
  //  mode = position;
  //  DisplayMode();
  //}
}

uint8_t Input_getMode( void ){
  return mode;
}
float Input_getWheel( void ){
  return Wheel_getValue() / 4095.;
}