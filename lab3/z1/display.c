#include <stdio.h>
#include "display.h"
#include "HAL_Dogs102x6.h"

static float periods[2];
static float ks[2];
void Display_Init( void ) {
  Dogs102x6_init();
  Dogs102x6_backlightInit();
  Dogs102x6_clearScreen();
  Dogs102x6_setBacklight(3);
}
void Display(Display_TypeDef chan, float period, float k)
{
  periods[chan] = period;
  ks[chan] = k;
  
  Dogs102x6_clearScreen();
  
  char line[20];
  snprintf(line, 20, "P: %3.3f  %3.3f", periods[0], periods[1]);
  
  Dogs102x6_stringDraw(4, 0, line, DOGS102x6_DRAW_NORMAL);
  
  
  snprintf(line, 20, "K: %3.3f  %3.3f", ks[0], ks[1]);
  
  Dogs102x6_stringDraw(5, 0, line, DOGS102x6_DRAW_NORMAL);
  
  
  //Draw
}
