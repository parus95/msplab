#pragma once
#include <stdint.h>

void Input_Init( void );
void Input_Tick( void );

uint8_t Input_getMode( void );
float Input_getWheel( void );