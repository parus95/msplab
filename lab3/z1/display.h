#pragma once

typedef enum {
  Display_Generator,
  Display_Receiver
} Display_TypeDef;

void Display_Init( void );
void Display(Display_TypeDef chan, float period, float k);

