#include <stdbool.h>
#include <stdint.h>
#include <msp430.h>
#include "Receiver.h"

//TA2.1 - P2.4  - TA2.CCI1A

void Receiver_Init( void ){
  P2DIR &= ~BIT4;
  P2OUT |= BIT4;
  P2SEL |= BIT4;
  
  TA2CTL = TASSEL__SMCLK |  MC__UP | ID__4;
  TA2CCR0 = 0xFFFF;
  
  TA2CCTL1 = CCIS_0 | CM_3 | SCS | CAP | CCIE;
}

static struct ReceiverDataStruct{
  bool avail;
  uint16_t cnt;
  bool dir;  
} Receiver_ISRData;

typedef struct ReceiverDataStruct ReceiverData_TypeDef;

#pragma vector=TIMER2_A1_VECTOR
__interrupt void TIMER2_A1_ISR(void)
{
  const uint16_t cctl = TA2CCTL1;
  if (cctl & CCIFG){
    TA2CCTL1 &=~ CCIFG;
    
    if (!Receiver_ISRData.avail){
      Receiver_ISRData.avail = true;
      Receiver_ISRData.cnt = TA2CCR1;
      //Receiver_ISRData.dir = !!(cctl & CCI);
      Receiver_ISRData.dir = !!(P2IN & BIT4);
    }else{
      __no_operation();//Error 
    }
  }
}

static void IIR(float *v, float in)
{
  const float k = 0.95;
  *v = (k * (*v)) + (1 - k) * in;
}


static ReceiverData_TypeDef lastData;
static float timings[2];
static uint16_t updateCounter = 1;

void Receiver_Tick( void ){
  if (Receiver_ISRData.avail){
    ReceiverData_TypeDef newData = Receiver_ISRData;
    Receiver_ISRData.avail = false;

    if (newData.dir != lastData.dir){
      const uint16_t time = newData.cnt - lastData.cnt;
      IIR(&timings[newData.dir], time / 25000. * 4);
      if (!(updateCounter--)){
        updateCounter = 200;
        const float period = timings[0] + timings[1];
        Receiver_Callback(period, timings[0] / period);
      }
    }else{
      __no_operation();//Error
    }
    lastData = newData;
  }
}
