#include <msp430.h>
#include "stepper.h"

#define STEPPER_A_EN    4
#define STEPPER_A_DIR   1
#define STEPPER_B_EN    8
#define STEPPER_B_DIR   2

void Stepper_Init( void ) {
  P6DIR |= BIT6;
  P4DIR |= BIT6 | BIT7;
  P5DIR |= BIT1;
}

#define COPYBIT(destW, destB, srcW, srcB) \
if (srcW & srcB){    \
  destW |= destB;    \
} else {             \
  destW &= ~(destB); \
}

static inline void Stepper_WriteBits( uint8_t state ) {
  COPYBIT(P5OUT, BIT1, state, STEPPER_A_EN);
  COPYBIT(P4OUT, BIT7, state, STEPPER_A_DIR);
  COPYBIT(P4OUT, BIT6, state, STEPPER_B_EN);
  COPYBIT(P6OUT, BIT6, state, STEPPER_B_DIR);
}

void Stepper_Write( uint8_t pos ) {
  static const uint8_t conv[] = {0, 2, 3, 1};
  Stepper_WriteBits( conv[ pos & 3 ] | STEPPER_A_EN | STEPPER_B_EN );
}

void Stepper_Disable( void ) {
  Stepper_WriteBits(0);
}

