#pragma once
#include <stdint.h>
#include <stdbool.h>

void Cabine_Init( void );
void Cabine_GoToPosition( uint16_t pos );
void Cabine_SetDoorsOpened( bool v );

void Cabine_Tick( void );

uint16_t Cabine_GetPosition( void );
bool Cabine_GetDoorsOpened( void );
