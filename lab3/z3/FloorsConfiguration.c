#include "FloorsConfiguration.h"

uint16_t FloorToPosition( uint8_t floorN ) {
  return 300 * floorN;
}

uint8_t PositionToFloor( uint16_t position ) {
  position += 300 / 2;
  position /= 300;
  return position;
}
