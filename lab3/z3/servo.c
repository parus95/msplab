#include <msp430.h>
#include <stdint.h>

#include "servo.h"


#define TMRV(x) (25UL * ((x * 1UL) / 8) )
#define SERVO_C 1500
#define SERVO_A 1800

void Servo_Init( void ) {
  P2OUT |= BIT4;
  P2SEL |= BIT4;
  
  TA2CTL = TASSEL__SMCLK | ID__8 | MC__UP ;
  TA2CCR0 = TMRV(20000);
  TA2CCTL0 = 0;
  TA2CCR1 = TMRV(SERVO_C);
  TA2CCTL1 = CM_0 | OUTMOD_7;
}

void Servo_Write( float n ) {
  const float w =  n * SERVO_A + (SERVO_C - SERVO_A / 2);
  TA2CCR1 = TMRV((uint16_t)w);
}

