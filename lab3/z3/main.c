#include <stdint.h>
#include <stdbool.h>
#include <msp430.h>
#include "HAL_Buttons.h"

#include "HAL_Dogs102x6.h"
#include "CTS_Layer.h"

#include "HAL_Wheel.h"

#include "system.h"
#include "Cabine.h"
#include "FloorsConfiguration.h"
#include "HAL_AppUart.h"


static inline uint8_t GetSliderPosition(void)
{
  uint16_t sliderPosition = TI_CAPT_Slider(&slider);

  sliderPosition = (sliderPosition + 10) / 20;

  if (sliderPosition && sliderPosition < 6)
    return sliderPosition - 1;

  return 0xFF;
}

void Draw( void ){
  static uint8_t i = 1;
  if (--i){
    return;
  }
  
  i = 20;
  
  const uint8_t currentFloor = PositionToFloor(Cabine_GetPosition());
  Dogs102x6_clearScreen();
  
  Dogs102x6_charDraw(2, 5, '0' + currentFloor, DOGS102x6_DRAW_NORMAL);
  Dogs102x6_charDraw(3, 5, "CO"[Cabine_GetDoorsOpened()], DOGS102x6_DRAW_NORMAL);
}

int main( void ){
  WDTCTL = WDTPW + WDTHOLD;
  SystemSetup();
  Buttons_init(BUTTON_ALL);

  TI_CAPT_Init_Baseline(&slider);
  
  AppUart_init();
  Dogs102x6_init();
  Dogs102x6_backlightInit();
  Dogs102x6_clearScreen();
  Dogs102x6_setBacklight(3);

  
  Wheel_init();
  Wheel_enable();
  
  
  P7DIR &= ~BIT0;
  P7REN |= BIT0;
  P7OUT |= BIT0;
  
  Cabine_Init();
    
  
  while (true){
    __delay_cycles(70000UL);
    Cabine_Tick();
    
    if (!(P1IN & BIT7)){
      Cabine_SetDoorsOpened(true);
    }
    if (!(P2IN & BIT2)){
      Cabine_SetDoorsOpened(false);
    }
    
    const uint8_t pos = GetSliderPosition();
    if (pos != 0xFF ){
      Cabine_GoToPosition(FloorToPosition(pos));
    }
    
    if ((UCA1IFG & UCRXIFG) && (P7IN & BIT0)) {
      uint8_t receiveChar = UCA1RXBUF;
      if (receiveChar >= '0' && receiveChar <= '9'){
        Cabine_SetDoorsOpened(false);
        Cabine_GoToPosition(FloorToPosition(receiveChar - '0'));        
      }
    }
    
    Cabine_Tick();
    Draw();
    
    
    // Stepper_Write(state);
   // state+= ( PAIN & BIT7 ) ? 0x01 : 0xFF;
    //state &= 0x03;
    
    //Servo_Write(Wheel_getValue() / 4095.);
    
  }
  
}