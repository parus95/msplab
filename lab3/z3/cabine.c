#include <stdbool.h>
#include "cabine.h"

#include "servo.h"
#include "stepper.h" 

static uint16_t Cabine_CurrentPosition = 0, Cabine_RequiredPosition = 0;
static bool Cabine_IsOpened;
static uint8_t Cabine_StepperPosition = 0;

void Cabine_Init( void ) {
  Servo_Init();
  Stepper_Init();
}

void Cabine_GoToPosition( uint16_t pos ) {
    Cabine_RequiredPosition = pos;
}

void Cabine_SetDoorsOpened( bool v ) {
  // ��������� ����� ����� ������ �� ����������� �����
  if ((Cabine_CurrentPosition == Cabine_RequiredPosition ) || !v) {
    Servo_Write(v ? 0 : 1);
    Cabine_IsOpened = v;
  }
}

static bool Cabine_RateDelimiter( void ){
  return true;
}

void Cabine_Tick( void ) {
  static uint8_t doorDelay = 0;
  
  if (Cabine_GetDoorsOpened()){
    doorDelay = 200;
  } else if (doorDelay) {
    doorDelay--;
  }
  
  if (!Cabine_RateDelimiter() 
      || doorDelay
      || Cabine_CurrentPosition == Cabine_RequiredPosition){
    return;
  }
  bool dir = Cabine_RequiredPosition > Cabine_CurrentPosition;
  Cabine_StepperPosition += dir ? 1 : -1;
  Cabine_CurrentPosition += dir ? 1 : -1;
  Stepper_Write(Cabine_StepperPosition);
  if (Cabine_CurrentPosition == Cabine_RequiredPosition) {
    Cabine_SetDoorsOpened( true );
  }
}

uint16_t Cabine_GetPosition( void ) {
  return Cabine_CurrentPosition;
}

bool Cabine_GetDoorsOpened( void ) {
  return Cabine_IsOpened;
}
