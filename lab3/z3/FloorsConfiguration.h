#pragma once
#include <stdint.h>

uint16_t FloorToPosition( uint8_t floorN );
uint8_t PositionToFloor( uint16_t position );
