#pragma once
#include <stdint.h>

void Stepper_Init( void );
void Stepper_Write( uint8_t pos );
void Stepper_Disable( void );
