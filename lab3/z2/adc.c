#include <stdint.h>
#include <msp430.h>
#include "shr.h"

extern volatile uint16_t positionData;

#pragma vector = ADC12_VECTOR
__interrupt void ADC12_ISR(void)
{
    switch (__even_in_range(ADC12IV, ADC12IV_ADC12IFG15))
    {
        // Vector  ADC12IV_NONE:  No interrupt
        case  ADC12IV_NONE:
            break;

        // Vector  ADC12IV_ADC12OVIFG:  ADC overflow
        case  ADC12IV_ADC12OVIFG:
            break;

        // Vector  ADC12IV_ADC12TOVIFG:  ADC timing overflow
        case  ADC12IV_ADC12TOVIFG:
            break;

        // Vector  ADC12IV_ADC12IFG0: ADC12IFG0:
        case  ADC12IV_ADC12IFG0:
            positionData = ADC12MEM0;                  // ADC12MEM = A0 > 0.5AVcc?
           // __bic_SR_register_on_exit(LPM0_bits);      // Exit active CPU
            break;

        // Vector  ADC12IV_ADC12IFG1:  ADC12IFG1
        case  ADC12IV_ADC12IFG1:
          SHR_Input(ADC12MEM1);
            //__bic_SR_register_on_exit(LPM0_bits);
            break;

        // Vector ADC12IV_ADC12IFG2:  ADC12IFG2
        case ADC12IV_ADC12IFG2:
            break;

        // Vector ADC12IV_ADC12IFG3:  ADC12IFG3
        case ADC12IV_ADC12IFG3:
            break;

        // Vector ADC12IV_ADC12IFG4:  ADC12IFG4
        case ADC12IV_ADC12IFG4:
            break;

        // Vector ADC12IV_ADC12IFG5:  ADC12IFG5
        case ADC12IV_ADC12IFG5:
            break;

        // Vector ADC12IV_ADC12IFG6:  ADC12IFG6
        case ADC12IV_ADC12IFG6:
            break;

        // Vector ADC12IV_ADC12IFG7:  ADC12IFG7
        case ADC12IV_ADC12IFG7:
            break;

        // Vector ADC12IV_ADC12IFG8:  ADC12IFG8
        case ADC12IV_ADC12IFG8:
            break;

        // Vector ADC12IV_ADC12IFG9:  ADC12IFG9
        case ADC12IV_ADC12IFG9:
            break;

        // Vector ADC12IV_ADC12IFG10:  ADC12IFG10
        case ADC12IV_ADC12IFG10:
            break;

        // Vector ADC12IV_ADC12IFG11:  ADC12IFG11
        case ADC12IV_ADC12IFG11:
            break;

        // Vector ADC12IV_ADC12IFG12:  ADC12IFG12
        case ADC12IV_ADC12IFG12:
            break;

        // Vector ADC12IV_ADC12IFG13:  ADC12IFG13
        case ADC12IV_ADC12IFG13:
            break;

        // Vector ADC12IV_ADC12IFG14:  ADC12IFG14
        case ADC12IV_ADC12IFG14:
            break;

        // Vector ADC12IV_ADC12IFG15:  ADC12IFG15
        case ADC12IV_ADC12IFG15:
            break;

        default:
            break;
    }
}