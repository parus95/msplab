#include <string.h>
#include <msp430.h>
#include <stdbool.h>

#include "HAL_Wheel.h"

#include "System.h"
#include "shr.h"

#include "HAL_Board.h"
#include "HAL_Dogs102x6.h"
/*

L-53P3C
Long  - emitter
Short - collector


P6.6 - A6






*/

#pragma vector=TIMER2_A0_VECTOR
__interrupt void TIMER2_A0_ISR( void )
{
__no_operation();//error
}

#pragma vector=TIMER2_A1_VECTOR
__interrupt void TIMER2_A1_ISR( void )
{
  TA2CTL &=~ TAIFG;  
  ADC12CTL0 |= ADC12SC;
}

int main( void )
{
  // Stop watchdog timer to prevent time out reset
  WDTCTL = WDTPW + WDTHOLD;

  SystemSetup();
  
  Wheel_init();
  Wheel_enable();
  ADC12CTL0 &= ~(ADC12ON | ADC12ENC);
  
  ADC12MCTL1 = ADC12INCH_6;
  ADC12MCTL2 = ADC12EOS;
  
  ADC12CTL1 |= ADC12CONSEQ_1;
  ADC12CTL0  |= ADC12MSC;
  
  ADC12CTL0 |= (ADC12ON | ADC12ENC);
  
  P6OUT &= ~BIT6;
  P6DIR &= ~BIT6;
  P6SEL |=  BIT6;
  
  P4DIR |= BIT7;
  TA2CTL = TASSEL__SMCLK |  MC__UP | ID__8 | TAIE;
  TA2CCTL0 = 0;
  TA2CCR0 = 25000000UL/8/2/460;

  ADC12IE |= ADC12IE0 | ADC12IE1;

  __enable_interrupt();
  
  char line[40] = "";
  
  Dogs102x6_init();
  Dogs102x6_backlightInit();
  Dogs102x6_clearScreen();
  Dogs102x6_setBacklight(3);
  
    Board_ledOn( LED1 );
  while (true){
    if (SHR_Avail){        
      Board_ledOff(LED1 | LED2 | LED3);
      Board_ledOn( 1 << (SHR_Value+1));
      
      if (strlen(line)>=30){
        line[0] = 0;
      }
      
      strncat(line, SHR_Value ? "1" : "0", 40);
      
      SHR_Avail = false;
      
      Dogs102x6_clearScreen();
      Dogs102x6_stringDraw(4, 0, line, DOGS102x6_DRAW_NORMAL);
    }
    __delay_cycles(1000UL);
  }
}
