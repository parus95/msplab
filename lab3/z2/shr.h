#pragma once
#include <stdint.h>
#include <stdbool.h>

#define M_LENGTH 15

#define SHR_LENGTH (2 * M_LENGTH - 1)

extern volatile bool SHR_Value, SHR_Avail;

void SHR_Input(uint16_t v);



