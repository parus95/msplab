#include <msp430.h>
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>
#include "shr.h"

extern volatile uint16_t positionData;

static volatile uint16_t SHR_Data[SHR_LENGTH];
static volatile uint8_t SHR_Locked = SHR_LENGTH + 2;

bool volatile SHR_Value = false, SHR_Avail = false;

const uint16_t poly = 
0
//M-code
|(0 << 0)
|(1 << 1)
|(1 << 2)
|(1 << 3)
|(0 << 4)
|(1 << 5)
|(1 << 6)
|(0 << 7)
|(0 << 8)
|(1 << 9)
|(0 << 10)
|(1 << 11)
|(0 << 12)
|(0 << 13)
|(0 << 14)
;

void SHR_Input(uint16_t v)
{
  for (uint8_t i= SHR_LENGTH -1 ; i > 0; i--)
  {
    SHR_Data[i] = SHR_Data[i-1];
  }
  SHR_Data[0] = v;
  
  if (v > 2048){
    P4OUT |= BIT7;
  }else{
    P4OUT &= ~BIT7;    
  }
  
  if (SHR_Locked){
    SHR_Locked--;
  } else {
    int32_t s = 0;
    int32_t sum = 0;
    uint16_t p = poly;
    for (uint8_t i=0; i< SHR_LENGTH; i+=2, p >>= 1){
      const uint16_t data = SHR_Data[SHR_LENGTH - 1 - i];
      sum += data;
      if (p & 1){
        s += data;
      } else {
        s -= data;      
      }
    }
    s /= M_LENGTH;
    sum /= M_LENGTH;

    s += sum / M_LENGTH;

    if (abs(s) > positionData) {
      SHR_Locked = 1;      
      SHR_Value =  s > 0;
      SHR_Avail = true;
    }
    
  }
}


